# GetAGriffin Product Suite

This repo is the product level repo, which integrates various components into a
user-facing, value-producing product. It contains:

* Vagrantfile - for standup up a local VM that can be used for local functional
integration testing.
* toolchain-vm.xml - A toolchain configuration for the above VM. The Vagrantfile
automatically install this file in the VM.
* nginx.conf - An nginx configuration for standing up a reverse proxy to put Jenkins
behind SSL/TLS.
* Product level test suites (implemented as Maven modules and run by Cucumber),
per the test strategy (see below).

## Component Repos

This product is composed of component, contained in the following component repos:

* UI
* Order
* FulfillGriffin
* FulfillSaddle

Each component repo has a component level test strategy, for that component. However,
a component should not be considered to be tested until its component level tests pass,
and the product level tests also pass for any products in which the component is used.

## Other Repos Needed

The following repo is needed for this product. It is not in Maven Central, so you will need
to clone it and build it, by running `make install`, which will build it and install it
in your local maven repository:

https://gitlab.com/cliffbdf/utilities-java

## To Deploy All components

### Locally


### In AWS


## To Run Test Suites


### Locally

### Under Jenkins


## Product Level Test Strategy

1. Integration test (thorough coverage):
	1. Order -> ((FulfillGriffin -> DB), (FulfillSaddle -> DB))
2. Integration test (light coverage):
	1. UI -> Order -> ((FulfillGriffin -> DB), (FulfillSaddle -> DB))
3. Failure mode test (local):
	1. Component failure: Order’s DB becomes unreachable
	2. Component failure: FulfillGriffin hangs
	3. Saga failure: Order update succeeds but FulfillGriffin update fails
4. Dynamic security scan (local)
5. Stress test (Jenkins)

### Component Level Tests

Each component repo contains test suites for that component. These include,

1. API test of microservice Order (with FulfillGriffin and FulfillSaddle mocked)
2. API test of microservice FulfillGriffin
3. API test of microservice FulfillSaddle

## External Packages used

The microservices in this product use the SparkJava framework, which is a much
lighterweight, less opinionated alternative to Spring. It is also easier for those
new to the framework to understand the code. The documentation for SparkJava
can be found here: http://sparkjava.com/documentation
