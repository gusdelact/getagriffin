# Product level tasks, including integration test tasks.

# Set the target environment; choices are vm or ec2:
#export ENV := ec2
export ENV := vm

.PHONY: test211 deployall destroyall commitall product fulfillgriffin fulfillsaddle order ui

# ------------------------------------------------------------------------------
# Integration test 2.1.1: deploy, run tests, destroy:
#	Order -> (FulfillGriffin -> DB), (FulfillSaddle -> DB)
#	This is merely Order’s behavioral tests (1.1), but with FulfillGriffin and FulfillSaddle
#	fully deployed.
test211complete: deploy test211 destroy

# Run test suite 2.1.1 as an integration test (no modks).
# Assumes that the product has already been deployed:
test211:
	{ \
	pushd ../GetAGriffin-Order; \
	. ./env.$${ENV}.source; \
	export ORDER_FACTORY_CLASS_NAME=order.stub.OrderFactoryLive; \
	export FULFILL_GRIFFIN_FACTORY_CLASS_NAME=fulfillgriffin.stub.FulfillGriffinFactoryLive; \
	export FULFILL_SADDLE_FACTORY_CLASS_NAME=fulfillsaddle.stub.FulfillSaddleFactoryLive; \
	./comptest.sh; \
	popd; \
	}

# Run the failure mode tests.
# Assumes that the product has already been deployed:
failtest:
	{ \
	. ./env.$${ENV}.source; \
	./failtest.sh; \
	}


# ------------------------------------------------------------------------------
# Reusable steps.

# Deploy all of the product's components:
deploy:
	{ \
	pushd ../GetAGriffin-FulfillGriffin; \
	. ./env.$${ENV}.source; \
	export FULFILL_GRIFFIN_MYSQL_HOST_PORT=3001; \
	./deploy.vm.sh -d; \
	popd; \
	}
	{ \
	pushd ../GetAGriffin-FulfillSaddle; \
	. ./env.$${ENV}.source; \
	export FULFILL_SADDLE_MYSQL_HOST_PORT=3002; \
	./deploy.vm.sh -d; \
	popd; \
	}
	{ \
	pushd ../GetAGriffin-Order; \
	. ./env.$${ENV}.source; \
	export FULFILL_GRIFFIN_SERVICE_URL=http://fulfillgriffin:2001; \
	export FULFILL_SADDLE_SERVICE_URL=http://fulfillsaddle:2002; \
	export ORDER_MYSQL_HOST_PORT=3003; \
	export FULFILL_GRIFFIN_FACTORY_CLASS_NAME=fulfillgriffin.stub.FulfillGriffinFactoryLive; \
	export FULFILL_SADDLE_FACTORY_CLASS_NAME=fulfillsaddle.stub.FulfillSaddleFactoryLive; \
	./deploy.vm.sh -d; \
	popd; \
	}

# Destroy (undeploy) all of the product's components:
destroy:
	{ \
	pushd ../GetAGriffin-FulfillGriffin; \
	. ./env.$${ENV}.source; \
	./destroy.vm.sh; \
	}
	{ \
	pushd ../GetAGriffin-FulfillSaddle; \
	. ./env.$${ENV}.source; \
	./destroy.vm.sh; \
	}
	{ \
	pushd ../GetAGriffin-Order; \
	. ./env.$${ENV}.source; \
	./destroy.vm.sh; \
	}

# Commit all changes to all of the product's repos.
commit:
	{ \
	git add . ; \
	git commit -am misc; \
	git push; \
	}
	{ \
	pushd ../GetAGriffin-FulfillGriffin; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	{ \
	pushd ../GetAGriffin-FulfillSaddle; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	{ \
	pushd ../GetAGriffin-Order; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	{ \
	pushd ../GetAGriffin-UI; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}

# Pull changes from all of the product's repos.
pull:
	{ \
	git pull; \
	}
	{ \
	pushd ../GetAGriffin-FulfillGriffin; \
	git pull; \
	popd; \
	}
	{ \
	pushd ../GetAGriffin-FulfillSaddle; \
	git pull; \
	popd; \
	}
	{ \
	pushd ../GetAGriffin-Order; \
	git pull; \
	popd; \
	}
	{ \
	pushd ../GetAGriffin-UI; \
	git pull; \
	popd; \
	}

# Build the product, consting of each component:
product: fulfillgriffin fulfillsaddle order ui
	{ \
	#. ./env.$${ENV}.source; \
	#./build.sh; \
	}

# Build the FulfillGriffin component image:
fulfillgriffin:
	{ \
	pushd ../GetAGriffin-FulfillGriffin; \
	. ./env.$${ENV}.source; \
	./build.sh; \
	popd; \
	}

# Build the FulfillSaddle component image:
fulfillsaddle:
	{ \
	pushd ../GetAGriffin-FulfillSaddle; \
	. ./env.$${ENV}.source; \
	./build.sh; \
	popd; \
	}

# Build the Order component image:
order:
	{ \
	pushd ../GetAGriffin-Order; \
	. ./env.$${ENV}.source; \
	./build.sh; \
	popd; \
	}

# Build the FulfillGriffin component image:
ui:
	{ \
	pushd ../GetAGriffin-UI; \
	#. ./env.$${ENV}.source; \
	#./build.sh; \
	popd; \
	}

x:
	{ \
	. ./x.source; \
	export XYZ=123; \
	echo $${DEF}; \
	echo $${XYZ}; \
	export CMD=y; \
	./$${CMD}.sh; \
	}
